#!/usr/bin/env bash

set -Eeuo pipefail

PORTS=( 10000 )
mapfile -t PORTS -O "${#PORTS[@]}" < <(
  httpd -t -D DUMP_CONFIG 2>/dev/null \
    | grep -i "^listen" \
    | cut -d " " -f2 \
    | while read -r PORT ; do
        echo "${PORT##*:}"
      done
)

/usr/local/sbin/healthcheck.sh "${PORTS[@]}"
