FROM registry.gitlab.com/craynic.com/craynic.net/mvh/php-fpm:4.0.5-php8.1@sha256:1cbe6d4158bfb7d25f359950b8ea50accae8ca44379df05773caa5bee3982817

# renovate: datasource=repology depName=alpine_3_21/apache2 depType=dependencies versioning=loose
ARG APACHE2_VERSION="2.4.62-r0"

RUN apk --no-cache add shadow~=4 \
    && usermod -d /var/www/ www-data \
    && apk --no-cache add \
        apache2="${APACHE2_VERSION}" \
        apache2-proxy="${APACHE2_VERSION}" \
        apache2-ssl="${APACHE2_VERSION}" \
    && rm -rf /var/www/html \
    && mkdir /var/www/htdocs /var/www/tmp /var/www/home \
    && chown -R www-data:www-data /var/www \
    && chmod -R u+rwX,g+rXs,o-rwx /var/www \
    && chmod g-rx /var/www/home

COPY files/ /

RUN sed -i "s/\(LoadModule\) \([[:alpha:]_]\+\) modules\/\([[:alpha:]_\/.]\+\)$/\1 \2 \/usr\/lib\/apache2\/\3/g" \
    /etc/apache2/httpd.conf \
    /etc/apache2/conf.d/*.conf

ENV LAP_MAX_EXECUTION_TIME=30 \
    LAP_PHP_PM_MAX_CHILDREN=5 \
    LAP_PHP_PM_START_SERVERS=1 \
    LAP_PHP_PM_MIN_SPARE_SERVERS=1 \
    LAP_PHP_PM_MAX_SPARE_SERVERS=3 \
    LAP_PHP_PM_MAX_REQUESTS=200 \
    LAP_PHP_MAX_MEMORY="32M"

WORKDIR /var/www/htdocs

EXPOSE 80 443

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"
