# Apache 2.4 - PHP 8.1-8.4

This image contains LAP hosting with the support for multiple PHP versions from 8.1 to 8.4.

## Environment variables

* `LAP_MAX_EXECUTION_TIME`: value for PHP-FPM's configuration of `max_execution_time`; defaults to 30
* `LAP_PHP_PM_MAX_CHILDREN`: value for PHP-FPM's configuration of `pm.max_children`; defaults to 5
* `LAP_PHP_PM_START_SERVERS`: value for PHP-FPM's configuration of `pm.start_servers`; defaults to 1
* `LAP_PHP_PM_MIN_SPARE_SERVERS`: value for PHP-FPM's configuration of `pm.min_spare_servers`; defaults to 1
* `LAP_PHP_PM_MAX_SPARE_SERVERS`: value for PHP-FPM's configuration of `pm.max_spare_servers`; defaults to 3
* `LAP_PHP_PM_MAX_REQUESTS`: value for PHP-FPM's configuration of `pm.max_requests`; defaults to 200
* `LAP_PHP_MAX_MEMORY`: value for PHP-FPM's configuration of `max_memory`; defaults to "32M"

### Mail support

To enable e-mail support out-of-the-box, please specify
the following ENV variables:

* `MAIL_RELAYHOST`: relay host for e-mail support; if not set,
    mail support is disabled.
* `MAIL_TLS_KEYFILE`: Path to a file holding the private key for
    client authentication. Optional; if specified,
    `MAIL_TLS_CRTFILE` must be set as well.
* `MAIL_TLS_CRTFILE`: Path to a file holding the public certificate
    for client authentication. Optional; if specified,
    `MAIL_TLS_KEYFILE` must be set as well.
* `MAIL_TLS_CAFILE`: Path to a file holding the CA certificate
    for server verification.

## Usage and examples

### Local development with a mapped volume

This will start the server with a volume `/local/dir` attached:

```bash
docker run -dit \
    -p 80:80 \
    -v /local/dir/:/var/www/htdocs/ \
    --rm --name docker-lap \
    registry.gitlab.com/craynic.com/docker/lap:latest
```
